export interface Books {
  accessInfo: {
    country: string;
    viewability: string;
    embeddable: true;
    publicDomain: false;
    textToSpeechPermission: string;
  };
  etag: string;
  id: string;
  kind: string;
  saleInfo: {
    country: string;
    saleability: string;
    isEbook: true;
    listPrice?: {amount: number; currencyCode: number};
    retailPrice: {amount: number; currencyCode: number};
  };
  searchInfo: {textSnippet: string};
  selfLink: string;
  volumeInfo: VolumeInfo;
}

export type VolumeInfo = {
  allowAnonLogging: boolean;
  authors: string[];
  canonicalVolumeLink: string;
  categories: string[];
  contentVersion: string;
  description: string;
  imageLinks: {
    smallThumbnail: string;
    thumbnail: string;
  };
  infoLink: string;
  language: string;
  maturityRating: string;
  pageCount: number;
  panelizationSummary: {
    containsEpubBubbles: boolean;
    containsImageBubbles: boolean;
  };
  previewLink: string;
  printType: string;
  publishedDate: string;
  publisher: string;
  readingModes: {text: boolean; image: boolean};
  subtitle: string;
  title: string;
};
