import {useEffect, useState} from 'react';
import * as geolib from 'geolib';
import Geolocation from 'react-native-geolocation-service';
import {Notifications} from 'react-native-notifications';

import {useRequestLocationPermission} from './useRequestLocationPermission';
import {postLocalNotifications, useNotifications} from './useNotifications';

export function useGeoFance() {
  const [bookStoreName, setbookStoreName] = useState<string>('');

  const RADIUS = 400;
  type GeoFanceCoordinates = {
    latitude: number;
    longitude: number;
    bookstore_name: string;
  };

  const coordinates: GeoFanceCoordinates[] = [
    {
      latitude: -23.560331,
      longitude: -46.6399674,
      bookstore_name: 'Livraria Saraiva',
    },
    {
      latitude: -23.5591532,
      longitude: -46.6599876,
      bookstore_name: 'Livraria Cultura',
    },
  ];

  const localNotification = postLocalNotifications(bookStoreName);

  useEffect(() => {
    useRequestLocationPermission().then(granted => {
      if (granted) {
        Geolocation.watchPosition(position => {
          for (const coords of coordinates) {
            const inPosition = geolib.isPointWithinRadius(
              {latitude: coords.latitude, longitude: coords.longitude},
              {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
              },
              RADIUS,
            );
            console.log(inPosition);
            if (inPosition) {
              localNotification;
              setbookStoreName(coords.bookstore_name);
            } else {
              Notifications.removeAllDeliveredNotifications();
            }
          }
        });
      }
    });
  }, []);
}
