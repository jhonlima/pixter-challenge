import {Notifications, Notification} from 'react-native-notifications';
import {useEffect, useMemo} from 'react';
import {NotificationCompletion} from 'react-native-notifications/lib/dist/interfaces/NotificationCompletion';

export function useNotifications() {
  useEffect(() => {
    Notifications.registerRemoteNotifications();

    Notifications.events().registerNotificationReceivedForeground(
      (notification: Notification, completion) => {
        console.log(
          `Notification received in foreground: ${notification.title} : ${
            notification.body
          }`,
        );
        completion({alert: true, sound: true, badge: false});
      },
    );

    Notifications.events().registerNotificationOpened(
      (notification, completion) => {
        console.log(`Notification opened: $`);
        completion();
      },
    );
    Notifications.events().registerNotificationReceivedBackground(
      (
        notification: Notification,
        completion: (response: NotificationCompletion) => void,
      ) => {
        console.log('Notification Received - Background', notification.payload);

        // Calling completion on iOS with `alert: true` will present the native iOS inApp notification.
        completion({alert: true, sound: true, badge: false});
      },
    );
  }, []);
}

export function postLocalNotifications(bookstore_name: string) {
  let localNotification = useMemo(() => {
    Notifications.postLocalNotification(
      {
        body: 'Pixter Books  ',
        title: `Buy your favorite books now, ${bookstore_name} is near here`,
        sound: 'chime.aiff',
        thread: '',
        badge: 2,
        identifier: `${bookstore_name}`,
        type: 'notificaion',
        payload: '',
      },
      1,
    );
  }, [bookstore_name]);
  return localNotification;
}
