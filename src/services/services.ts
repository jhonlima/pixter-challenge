import axios from 'axios';

//base Api
const BASE_URL = 'https://www.googleapis.com/books/v1/';

const endPoints = {
  getBooksEndPoint: (search: string, page: number) =>
    `volumes?q=${search}&startIndex=${page}&maxResults=12`,
};

const booksApi = axios.create({
  baseURL: BASE_URL,
  timeout: 60000,
  headers: {'Content-type': 'application/json'},
});

const apiService = {
  getBooks: (search: string, page: number) =>
    booksApi.get(endPoints.getBooksEndPoint(search, page)),
};

export default apiService;
