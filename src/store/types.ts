import {rootReducer} from './index';
import {Action} from 'redux';
import {ThunkAction} from 'redux-thunk';

export type StoreState = ReturnType<typeof rootReducer>;

export interface PixterAction<T> extends Action<string> {
  type: string;
  payload?: T;
}

export type PixterThunk<S, R = void | Promise<void>> = ThunkAction<
  R,
  StoreState,
  any,
  PixterAction<Partial<S>>
>;
