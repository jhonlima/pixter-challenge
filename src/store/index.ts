import {createStore, applyMiddleware, combineReducers} from 'redux';
import thunk from 'redux-thunk';

import booksReducer from './books/reducer';

export const rootReducer = combineReducers({
  books: booksReducer,
});

export default function configureStore(initialState = {}) {
  const middlewares = [thunk];
  const store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(...middlewares),
  );
  return store;
}
