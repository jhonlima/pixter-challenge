import {Books} from './../../contract/books';
import {PixterThunk} from './../types';
import {BookState, GET_ALL_BOOKS} from './types';
import apiService from '../../services/services';
import {Alert} from 'react-native';

export function getAllBooks(
  search: string,
  page: number,
): PixterThunk<BookState> {
  return dispatch => {
    apiService
      .getBooks(search, page)
      .then(response => {
        const books = response.data.items as Books[];
        dispatch({
          type: GET_ALL_BOOKS,
          payload: {books},
        });
      })
      .catch(err => Alert.alert('Erro', 'Tente novamente mais tarde'));
  };
}
