import {BookState, GET_ALL_BOOKS} from './types';
import {AnyAction} from 'redux';

const initialState: BookState = {
  books: [],
};

const booksReducer = (
  state: BookState = initialState,
  {type, payload}: AnyAction,
): BookState => {
  switch (type) {
    case GET_ALL_BOOKS:
      return {...state, ...payload};
    default:
      return state;
  }
};

export default booksReducer;
