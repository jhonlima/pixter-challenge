import {Books} from './../../contract/books';

export const GET_ALL_BOOKS = 'pixter.get.all.books';

export interface BookState {
  books?: Books[];
}
