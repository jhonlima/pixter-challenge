import React from 'react';
import {StatusBar} from 'react-native';
import {Provider} from 'react-redux';

import colors from './misc/colors';
import Routes from './navigation/Routes';
import configureStore from './store';
import {useGeoFance} from './hooks/useGeoFance';
import {useNotifications} from './hooks/useNotifications';

const store = configureStore();

export default function Root() {
  useNotifications();
  useGeoFance();
  return (
    <Provider store={store}>
      <StatusBar backgroundColor={colors.yellow} barStyle="dark-content" />
      <Routes />
    </Provider>
  );
}
