export default {
  verySmall: {
    shadowColor: 'rgba(184, 118, 12, 0.725119)',
    shadowOffset: {
      width: 2,
      height: 10,
    },
    shadowOpacity: 0.53,
    shadowRadius: 8.62,
    elevation: 4,
  },
  small: {
    shadowColor: 'rgba(184, 118, 12, 0.725119)',
    shadowOffset: {
      width: 2,
      height: 20,
    },
    shadowOpacity: 0.53,
    shadowRadius: 8.62,
    elevation: 4,
  },
  large: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 5,
  },
};
