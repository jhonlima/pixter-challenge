const colors = {
  yellow: '#FFDD0D',
};
export default {
  yellow: '#FFDD0D',
  secondYellow: '#F0D10F',
  black: '#000',
  fontPrimary: '#9F8B0C',
  bluPrimary: '#4A90E2',
  white: '#ffff',
  red: '#DC4B5D',
  blackSecondary: '#2C2605',
};
