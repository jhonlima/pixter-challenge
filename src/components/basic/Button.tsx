import React from 'react';
import {
  Text,
  StyleSheet,
  ViewStyle,
  TouchableOpacity,
  TextStyle,
} from 'react-native';
import colors from '../../misc/colors';
import shadows from '../../misc/shadows';
import fonts from '../../misc/fonts';

interface Props {
  title: string;
  onPress(): void;
}

export default function Button(props: Props) {
  return (
    <TouchableOpacity onPress={props.onPress} style={styles.buttonBuy}>
      <Text style={styles.txtButtonBuy}>{props.title}</Text>
    </TouchableOpacity>
  );
}

class Style {
  constructor(
    readonly buttonBuy: ViewStyle = {
      width: 116,
      height: 36,
      backgroundColor: colors.bluPrimary,
      borderRadius: 100,
      alignItems: 'center',
      justifyContent: 'center',
      ...shadows.verySmall,
      marginBottom: 20,
    },
    readonly txtButtonBuy: TextStyle = {
      color: colors.white,
      fontSize: 13,
      fontWeight: 'bold',
      fontFamily: fonts.regular,
    },
  ) {}
}

const styles = StyleSheet.create<Style>(new Style());
