import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ViewStyle,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import colors from '../../misc/colors';

interface Props {
  onPress(): void;
  like: boolean;
}

export default function ButtonLike(props: Props) {
  const color = props.like ? colors.white : colors.black;
  const backgroundColor = props.like ? colors.red : colors.white;

  function handleLike() {}
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={[styles.likeButton, {backgroundColor}]}>
      <Icon size={20} name="ios-heart-empty" color={color} />
    </TouchableOpacity>
  );
}

class Style {
  constructor(
    readonly likeButton: ViewStyle = {
      width: 36,
      height: 36,
      borderRadius: 18,
      alignItems: 'center',
      justifyContent: 'center',
      marginLeft: 8,
    },
  ) {}
}

const styles = StyleSheet.create<Style>(new Style());
