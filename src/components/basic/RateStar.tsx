import React from 'react';
import {View, StyleSheet, ViewStyle} from 'react-native';
import StarRating from 'react-native-star-rating';
import colors from '../../misc/colors';

interface Props {
  pressHate: (value: number) => void;
  rating: number;
}

export default function RateStar(props: Props) {
  return (
    <View style={styles.container}>
      <StarRating
        emptyStar={'ios-star-outline'}
        fullStar={'ios-star'}
        iconSet={'Ionicons'}
        maxStars={5}
        starSize={20}
        starStyle={{margin: 1}}
        rating={props.rating}
        selectedStar={props.pressHate}
        fullStarColor={colors.blackSecondary}
      />
    </View>
  );
}

class Style {
  constructor(
    readonly container: ViewStyle = {
      alignItems: 'center',
    },
  ) {}
}

const styles = StyleSheet.create<Style>(new Style());
