import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ViewStyle,
  TextStyle,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import colors from '../../misc/colors';

import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/MaterialIcons';
import {useNavigation} from '@react-navigation/native';

interface Props {
  search?: boolean;
  showIcon?: boolean;
  value?: string;
  onChangeText?: React.Dispatch<React.SetStateAction<string>>;
}

export default function Header(props: Props) {
  const [openSearch, setOpenSearch] = useState<boolean>(false);

  const navigate = useNavigation();
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        {props.showIcon ? (
          <TouchableOpacity
            style={styles.goBack}
            onPress={() => navigate.goBack()}>
            <Icon2 name="keyboard-backspace" size={30} />
          </TouchableOpacity>
        ) : (
          <View style={{width: 40}} />
        )}

        {openSearch ? (
          <TextInput
            style={styles.input}
            placeholder="Search Book"
            value={props.value}
            onChangeText={props.onChangeText}
          />
        ) : (
          <Text style={styles.txtTitle}>Pixter Books</Text>
        )}
        {!props.showIcon ? (
          <TouchableOpacity
            onPress={() => setOpenSearch(openSearch => !openSearch)}>
            <Icon name="ios-search" size={30} />
          </TouchableOpacity>
        ) : (
          <View style={{width: 40}} />
        )}
      </View>
      <View style={styles.line} />
    </View>
  );
}

class Style {
  constructor(
    readonly container: ViewStyle = {
      backgroundColor: colors.yellow,
      padding: 7,
    },
    readonly content: ViewStyle = {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    readonly txtTitle: TextStyle = {
      fontSize: 20,
      color: colors.black,
      textAlign: 'center',
    },
    readonly line: ViewStyle = {
      width: 119,
      backgroundColor: colors.secondYellow,
      height: 2,
      alignSelf: 'center',
      marginTop: 17,
    },
    readonly input: ViewStyle = {
      flex: 1,
      borderBottomWidth: 0.5,
      padding: 8,
      marginHorizontal: 40,
    },
    readonly goBack: ViewStyle = {
      height: 40,
      width: 40,
      alignItems: 'center',
      justifyContent: 'center',
    },
  ) {}
}

const styles = StyleSheet.create<Style>(new Style());
