import React from 'react';
import {
  SafeAreaView,
  RefreshControl,
  StatusBar,
  Image,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  StyleSheet,
  ImageStyle,
} from 'react-native';

import colors from '../../../misc/colors';
import Header from '../../basic/Header';
import { useNavigation } from '@react-navigation/native';
import { useLoadRepositories, useOnRefresh, useFetchDataBooks } from './hooks';


export default function Home() {

const {search, setSearch, loading} = useFetchDataBooks();

const { refreshing, onRefresh,} = useOnRefresh();

const { navigate } = useNavigation();

const {
  loadRepositories,
  books,
  
 } = useLoadRepositories();
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: colors.yellow, }}>
      <Header value={search} onChangeText={setSearch}/>
      <FlatList
        data={books}
        numColumns={3}
        contentContainerStyle={{alignItems: 'center', }}
        keyExtractor={item => item.etag}
        onEndReachedThreshold={0.1}
        onEndReached={loadRepositories}
        refreshControl={ 
          <RefreshControl 
            refreshing={refreshing} 
            onRefresh={onRefresh} 
          />}
        ListFooterComponent={renderFooter}
        renderItem={({item}) => (
          <TouchableOpacity onPress={()=> navigate('Details', { item })} >
            <Image
              style={styles.imageCard}
              source={{uri: item.volumeInfo.imageLinks?.thumbnail}}
            />
          </TouchableOpacity>
        )}
      />
    </SafeAreaView>
  );

  function renderFooter () {
    if (!loading) return null;
    return (
        <ActivityIndicator size='large'  />
    );
  };
}
class Style {
  constructor(
    readonly imageCard: ImageStyle = {
      width: 100, 
      height: 130,
      flex: 1,
      flexDirection: 'column',
      margin: 10
    }
  ) {}
}
const styles = StyleSheet.create<Style>(new Style())
