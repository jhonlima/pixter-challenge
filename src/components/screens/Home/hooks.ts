import {Books} from './../../../contract/books';
import {StoreState} from './../../../store/types';
import React, {useState, useCallback, useEffect} from 'react';

import {getAllBooks} from '../../../store/books/actions';
import {useDispatch, useSelector} from 'react-redux';

export function useFetchDataBooks() {
  const [search, setSearch] = useState<string>('React-native');
  const [page, setPage] = useState<number>(0);
  const [loading, setLoading] = useState<boolean>(false);

  const dispatch = useDispatch();

  useEffect(() => {
    setLoading(true);
    fetchAllbooks();
    setLoading(false);
  }, [search]);

  function fetchAllbooks() {
    dispatch(getAllBooks(search, page));
    setPage(page + 1);
  }

  return {
    search,
    setSearch,
    fetchAllbooks,
    loading,
  };
}

export function useLoadRepositories() {
  const [books, setBooks] = useState<Books[]>([]);

  const items = useSelector((state: StoreState) => state.books.books!);

  useEffect(() => {
    loadRepositories();
  }, [items]);

  function loadRepositories() {
    setBooks([...books, ...items]);
  }

  return {
    loadRepositories,
    books,
  };
}

export function useOnRefresh() {
  const [refreshing, setRefreshing] = React.useState(false);

  const {fetchAllbooks} = useFetchDataBooks();

  const onRefresh = useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => {
      fetchAllbooks();
      setRefreshing(false);
    });
  }, [refreshing]);

  function wait(timeout: number) {
    return new Promise(resolve => {
      setTimeout(resolve, timeout);
    });
  }
  return {
    refreshing,
    onRefresh,
  };
}
