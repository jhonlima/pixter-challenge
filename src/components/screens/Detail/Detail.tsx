import React, { useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ViewStyle,
  SafeAreaView,
  Image,
  TextStyle,
  ScrollView,
  Linking,
} from 'react-native';
import { useRoute } from '@react-navigation/native';
import { Books } from '../../../contract/books';
import colors from '../../../misc/colors';
import Header from '../../basic/Header';

import shadows from '../../../misc/shadows';
import fonts from '../../../misc/fonts';
import Button from '../../basic/Button';
import ButtonLike from '../../basic/ButtonLike';
import RateStar from '../../basic/RateStar';

interface Props {}

export default function Detail(props: Props) {
  const [rate, setRate] = useState<number>(0)
  const [like, setLike] = useState<boolean>(false);

  const route = useRoute();
  type Item = { item: Books };
  const details = route.params! as Item;

  const item = details.item
 function _valueChanged(rating: number) {
    setRate(rating)
  }

  return (
    <View style={ styles.container }>
      <SafeAreaView style={{ backgroundColor: colors.yellow }} />
      <Header showIcon />
      <View style={ styles.content }>
        <View style={{ marginHorizontal: 18 }}>
          <View style={ styles.viewContainer }>
            <View style={{ ...shadows.small }}>
              <Image
                resizeMode="contain"
                style={{ width: 100, height: 130 }}
                source={{ uri: item.volumeInfo.imageLinks.thumbnail }}
              />
            </View>
            <View style={ styles.viewTitleContent }>
              <Text  style={ styles.txtTitle }>
                { item.volumeInfo.title }
              </Text>
              <Text style={ styles.txtAuthors }>
                by { item.volumeInfo.authors }
              </Text>
              <View style={{marginTop: 17, flexDirection: "row", alignItems: 'center', }}>
              <Text style={styles.txtAmount}>
                {item.saleInfo.listPrice?.amount !== undefined
                  ? `$${item.saleInfo.listPrice!.amount}`
                  : 'Free'}
              </Text>
              <RateStar rating={rate} pressHate={_valueChanged}/>
              </View>
            </View>
          </View>
        </View>

        <Text style={styles.txtPageCount}>
          {item.volumeInfo.pageCount} Pages
        </Text>

        <View style={styles.viewButtonContainer}>
          <Button onPress={()=> Linking.openURL(item.volumeInfo.infoLink)} title='Buy' />
          <ButtonLike onPress={() => setLike(setLike => !setLike)} like={like} />
        </View>
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{flex: 1, marginHorizontal: 17}}>
        <Text style={styles.viewDescription}>
          {item.volumeInfo.description}
        </Text>
      </ScrollView>
    </View>
  );
}

class Style {
  constructor(
    readonly container: ViewStyle = {
      flex: 1,
    },
    readonly content: ViewStyle = {
      backgroundColor: colors.yellow,
      paddingBottom: 40,
    },
    readonly viewButtonContainer: ViewStyle = {
      position: 'absolute',
      right: 10,
      bottom: 10,
      flexDirection: 'row',
    },
    readonly txtAmount: TextStyle = {
      fontSize: 20,
      fontWeight: 'bold',
      fontFamily: fonts.regular,
      marginRight: 10,
      color: colors.blackSecondary
    },
    readonly txtAuthors: TextStyle = {
      color: colors.fontPrimary,
      marginTop: 2,
      fontFamily: fonts.regular,
      paddingRight: 15,
    },
    readonly viewTitleContent: ViewStyle = {
      width: 202,
      marginLeft: 23,
    },
    readonly txtPageCount: TextStyle = {
      fontSize: 14,
      color: colors.fontPrimary,
      marginTop: 30,
      fontFamily: fonts.regular,
      marginLeft: 36,
    },
    readonly viewContainer: ViewStyle = {
      flexDirection: 'row',
      marginTop: 10,
      alignItems: 'center',
    },
    readonly viewDescription: TextStyle = {
      fontSize: 14,
      marginTop: 31,
      lineHeight: 30,
      paddingBottom: 10,
    },
    readonly txtTitle: TextStyle = {
      fontSize: 20,
      fontFamily: fonts.regular,
      paddingRight: 15,
      color: colors.blackSecondary
    },
  ) {}
}

const styles = StyleSheet.create<Style>(new Style());
